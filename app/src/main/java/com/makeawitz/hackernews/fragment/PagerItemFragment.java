package com.makeawitz.hackernews.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.makeawitz.hackernews.MainActivity;
import com.makeawitz.hackernews.R;
import com.makeawitz.hackernews.recycler_adapter.TopicAdapter;
import com.makeawitz.hackernews.constant.GlobalConstants;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

public class PagerItemFragment extends Fragment {

    public FrameLayout frameLayout;
    private RecyclerView recycler;
    private SwipeRefreshLayout refresher;
    private LinearLayoutManager linearLayoutManager;
    private ContentManager contentManager;
    private TopicAdapter adapter;
    private List<String> topicIdList;
    private List<String> dataList;
    private boolean isFetchingData;
    private RequestQueue queue;
    private MainActivity activity;

    public PagerItemFragment() {
        // Required empty public constructor
    }

    public static PagerItemFragment newInstance() {
        return new PagerItemFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        frameLayout = (FrameLayout) inflater.inflate(R.layout.fragment_pager_item, container, false);
        recycler = (RecyclerView) frameLayout.findViewById(R.id.recycler);
        refresher = (SwipeRefreshLayout) frameLayout.findViewById(R.id.refresher);
        refresher.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                reloadContent();
            }
        });
        linearLayoutManager = new LinearLayoutManager(getContext());
        topicIdList = new ArrayList<>();
        recycler.setLayoutManager(linearLayoutManager);
        recycler.setAdapter(adapter);

        return frameLayout;
    }

    public PagerItemFragment init(MainActivity activity, String dataSrc) {
        setupPagerContent(dataSrc);
        isFetchingData = false;
        this.activity = activity;

        // Instantiate the RequestQueue.
        queue = Volley.newRequestQueue(activity);
        dataList = new ArrayList<>();
        adapter = new TopicAdapter(activity, this);
        return this;
    }

    public PagerItemFragment reloadContent() {
        if (contentManager != null) {
            if (refresher != null) {
                refresher.setRefreshing(true);
                adapter.clearList();
            }
            contentManager.reloadContent();
        }
        return this;
    }

    public void setTopicIdList(List<String> topicIdList) {
        this.topicIdList = topicIdList;
        dataList.clear();
        isFetchingData = true;
        requestContentList(GlobalConstants.EXTENSION_JSON, GlobalConstants.FIRST_DATA_SET, true);
    }

    public void insertMoreData() {
        dataList.clear();
        isFetchingData = true;
        requestContentList(GlobalConstants.EXTENSION_JSON, GlobalConstants.FETCH_MORE_AMOUNT, false);
    }

    public void refreshFinish() {
        if (refresher != null) {
            refresher.setRefreshing(false);
        }
    }

    public void viewComment(String title, String website, String id) {
        contentManager.viewComment(title, website, id);
    }

    private void requestContentList(final String extension, final int counter, final boolean firstFlag) {
        if (counter > 0 && topicIdList.size() > 0) {
            String url = GlobalConstants.API_URL + "item/" + topicIdList.get(topicIdList.size() - 1) + extension;
            topicIdList.remove(topicIdList.size() - 1);
            // Request a string response from the provided URL.
            StringRequest stringRequest = new StringRequest(
                    Request.Method.GET,
                    url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            dataList.add(response);
                            requestContentList(extension, counter - 1, firstFlag);
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            error.printStackTrace();
                        }
                    });
            // Add the request to the RequestQueue.
            queue.add(stringRequest);
        } else {
            if (topicIdList.size() <= 0) {
                // Completely loaded all topics
                adapter.noMore();
            }

            isFetchingData = false;
            refreshFinish();
            if (firstFlag) {
                adapter.setDataList(dataList);
            } else {
                adapter.insertNewData(dataList);
            }
        }
    }

    public boolean isFetchingData() {
        return isFetchingData;
    }

    public void setupPagerContent(final String dataSrc) {
        contentManager = new ContentManager() {
            @Override
            public void reloadContent() {
                // Request data
                String url = GlobalConstants.API_URL + dataSrc + GlobalConstants.EXTENSION_JSON;
                StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                try {
                                    JSONArray array = new JSONArray(response);
                                    List<String> topicIds = new ArrayList<>();
                                    for (int i = 0; i < array.length(); i++) {
                                        topicIds.add(0, array.getString(i));
                                    }
                                    PagerItemFragment.this.setTopicIdList(topicIds);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        PagerItemFragment.this.setTopicIdList(new ArrayList<String>());
                        //Toast.makeText(activity, error.getMessage(), Toast.LENGTH_LONG).show();
                    }
                });
                // Add the request to the RequestQueue.
                queue.add(stringRequest);
            }

            @Override
            public void viewComment(String title, String website, String id) {
                CommentItemFragment commentItemFragment = CommentItemFragment.newInstance();
                commentItemFragment.init(activity, title, website, null, id);
                activity.getSupportFragmentManager().beginTransaction()
                        .setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right)
                        .add(activity.commentContainer.getId(), commentItemFragment)
                        .addToBackStack(id)
                        .commit();
            }
        };
    }

    public interface ContentManager {
        void reloadContent();
        void viewComment(String title, String website, String id);
    }

}
