package com.makeawitz.hackernews.fragment;


import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.makeawitz.hackernews.MainActivity;
import com.makeawitz.hackernews.R;
import com.makeawitz.hackernews.constant.GlobalConstants;
import com.makeawitz.hackernews.recycler_adapter.CommentAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class CommentItemFragment extends Fragment {

    public FrameLayout frameLayout;
    private TextView title;
    private TextView replyTo;
    private View loading;
    private RecyclerView recycler;
    private LinearLayoutManager linearLayoutManager;
    private CommentAdapter adapter;
    private List<String> commentIdList;
    private List<String> dataList;
    private boolean isFetchingData;
    private RequestQueue queue;
    private String titleText;
    private String website;
    private String replyToText;

    public CommentItemFragment() {
        // Required empty public constructor
    }

    public static CommentItemFragment newInstance() {
        return new CommentItemFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        frameLayout = (FrameLayout) inflater.inflate(R.layout.fragment_comment, container, false);
        recycler = (RecyclerView) frameLayout.findViewById(R.id.recycler);
        title = (TextView) frameLayout.findViewById(R.id.title);
        replyTo = (TextView) frameLayout.findViewById(R.id.replyTo);
        loading = frameLayout.findViewById(R.id.loading);
        linearLayoutManager = new LinearLayoutManager(getContext());
        commentIdList = new ArrayList<>();
        recycler.setLayoutManager(linearLayoutManager);
        recycler.setAdapter(adapter);
        title.setText(titleText);
        title.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse(website));
                getActivity().startActivity(intent);
            }
        });
        if (replyToText != null) {
            replyTo.setVisibility(View.VISIBLE);
            String replyText = getResources().getString(R.string.repliedTo) + " " + replyToText;
            replyTo.setText(replyText);
        } else {
            replyTo.setVisibility(View.GONE);
        }
        loading.setVisibility(View.GONE);

        return frameLayout;
    }

    public void init(MainActivity activity, String title, String website, String replyTo, String id) {
        isFetchingData = false;
        this.titleText = title;
        this.website = website;
        this.replyToText = replyTo;
        // Instantiate the RequestQueue.
        queue = Volley.newRequestQueue(activity);
        dataList = new ArrayList<>();
        adapter = new CommentAdapter(activity, this);

        // Request data
        String url = GlobalConstants.API_URL + "item/" + id + GlobalConstants.EXTENSION_JSON;
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject json = new JSONObject(response);
                            JSONArray array = json.isNull("kids") ? new JSONArray() : json.getJSONArray("kids");
                            List<String> commentIds = new ArrayList<>();
                            for (int i = 0; i < array.length(); i++) {
                                commentIds.add(0, array.getString(i));
                            }
                            setCommentIdList(commentIds);
                            loading.setVisibility(View.GONE);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });
        // Add the request to the RequestQueue.
        queue.add(stringRequest);
    }

    public void setCommentIdList(List<String> commentIdList) {
        this.commentIdList = commentIdList;
        dataList.clear();
        isFetchingData = true;
        requestContentList(GlobalConstants.EXTENSION_JSON, GlobalConstants.FIRST_DATA_SET, true);
    }

    public void insertMoreData() {
        dataList.clear();
        isFetchingData = true;
        requestContentList(GlobalConstants.EXTENSION_JSON, GlobalConstants.FETCH_MORE_AMOUNT, false);
    }

    public void viewComment(String id, String replyTo, CommentItemFragment fragment) {
        MainActivity activity = (MainActivity) getActivity();
        fragment.init(activity, titleText, website, replyTo, id);
        activity.getSupportFragmentManager().beginTransaction()
                .setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right)
                .add(activity.commentContainer.getId(), fragment)
                .addToBackStack(id)
                .commit();

    }

    private void requestContentList(final String extension, final int counter, final boolean firstFlag) {
        if (counter > 0 && commentIdList.size() > 0) {
            String url = GlobalConstants.API_URL + "item/" + commentIdList.get(commentIdList.size() - 1) + extension;
            commentIdList.remove(commentIdList.size() - 1);
            // Request a string response from the provided URL.
            StringRequest stringRequest = new StringRequest(
                    Request.Method.GET,
                    url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            dataList.add(response);
                            requestContentList(extension, counter - 1, firstFlag);
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            error.printStackTrace();
                        }
                    });
            // Add the request to the RequestQueue.
            queue.add(stringRequest);
        } else {
            isFetchingData = false;
            if (commentIdList.size() <= 0) {
                // Completely loaded all topics
                adapter.noMore();
            }

            if (firstFlag) {
                adapter.setDataList(dataList);
            } else {
                adapter.insertNewData(dataList);
            }
        }
    }

    public boolean isFetchingData() {
        return isFetchingData;
    }

}
