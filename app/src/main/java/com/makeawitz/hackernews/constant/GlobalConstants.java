package com.makeawitz.hackernews.constant;

public class GlobalConstants {
    public static final String API_URL = "https://hacker-news.firebaseio.com/v0/";
    public static final int PAGER_AMOUNT = 3;
    public static final String EXTENSION_JSON = ".json";
    public static final int FIRST_DATA_SET = 30;
    public static final int FETCH_MORE_AMOUNT = 10;
    public static final int CHECK_BEFORE_DOWNLOAD = 20;
}
