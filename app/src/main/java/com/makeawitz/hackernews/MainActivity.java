package com.makeawitz.hackernews;

import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.FrameLayout;

import com.makeawitz.hackernews.constant.GlobalConstants;
import com.makeawitz.hackernews.fragment.PagerItemFragment;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    public FrameLayout commentContainer;
    public List<PagerItemFragment> pagerItemList;
    private ViewPager viewPager;
    private TabLayout pagerTabs;
    private PagerAdapter pagerAdapter;
    private ActionBar actionBar;
    private boolean isActionBarShowed;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        pagerTabs = (TabLayout) findViewById(R.id.pagerTabs);
        viewPager = (ViewPager) findViewById(R.id.viewPager);
        commentContainer = (FrameLayout) findViewById(R.id.commentContainer);
        pagerItemList = new ArrayList<>();

        // Setup, get resource JSON string list
        String[] tabTitles = getResources().getStringArray(R.array.pagerData);
        pagerAdapter = new ScreenSlidePagerAdapter(getSupportFragmentManager());
        for (int i = 0; i < tabTitles.length; i++) {
            PagerItemFragment pagerItemFragment = PagerItemFragment.newInstance();
            pagerItemList.add(pagerItemFragment);
        }
        viewPager.setAdapter(pagerAdapter);
        viewPager.setOffscreenPageLimit(GlobalConstants.PAGER_AMOUNT - 1);
        pagerTabs.setupWithViewPager(viewPager);
        for (int i = 0; i < tabTitles.length; i++) {
            try {
                JSONObject json = new JSONObject(tabTitles[i]);
                String title = json.isNull("title") ? "Tab" : json.getString("title");
                pagerTabs.getTabAt(i).setText(title);
                pagerItemList.get(i).init(MainActivity.this, json.getString("data_src")).reloadContent();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        isActionBarShowed = false;
        actionBar = getSupportActionBar();
    }
    
    @Override
    public boolean onSupportNavigateUp(){
        onBackPressed();
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (getSupportFragmentManager().getBackStackEntryCount() <= 0) {
            hideBackButton();
        }
    }

    public void showBackButton() {
        if (actionBar != null) {
            isActionBarShowed = true;
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowHomeEnabled(true);
        }
    }

    public void hideBackButton() {
        if (actionBar != null) {
            isActionBarShowed = false;
            actionBar.setDisplayHomeAsUpEnabled(false);
            actionBar.setDisplayShowHomeEnabled(false);
        }
    }

    public boolean isActionBarShowed() {
        return isActionBarShowed;
    }
    
    private class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter {
        private ScreenSlidePagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return pagerItemList.get(position);
        }

        @Override
        public int getCount() {
            return GlobalConstants.PAGER_AMOUNT;
        }
    }

}
