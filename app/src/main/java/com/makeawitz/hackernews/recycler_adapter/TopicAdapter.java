package com.makeawitz.hackernews.recycler_adapter;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.makeawitz.hackernews.MainActivity;
import com.makeawitz.hackernews.R;
import com.makeawitz.hackernews.constant.GlobalConstants;
import com.makeawitz.hackernews.fragment.PagerItemFragment;

import org.json.JSONException;
import org.json.JSONObject;
import org.ocpsoft.prettytime.PrettyTime;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class TopicAdapter extends RecyclerView.Adapter<TopicAdapter.CardViewHolder>{

    private MainActivity activity;
    private List<String> dataList;
    private PrettyTime prettyTime;
    private PagerItemFragment fragment;
    private boolean isNoMore;

    public TopicAdapter(MainActivity activity, PagerItemFragment fragment) {
        this.activity = activity;
        this.fragment = fragment;
        dataList = new ArrayList<>();
        prettyTime = new PrettyTime(Locale.ENGLISH);
        isNoMore = false;
    }

    public TopicAdapter(MainActivity activity, List<String> dataList, PagerItemFragment fragment) {
        this.activity = activity;
        this.fragment = fragment;
        this.dataList = dataList;
        prettyTime = new PrettyTime(Locale.ENGLISH);
        isNoMore = false;
    }

    public void setDataList(List<String> dataList) {
        if (dataList.size() > 0) {
            this.dataList.addAll(dataList);
            isNoMore = false;
            this.notifyDataSetChanged();
        }
    }

    public void clearList() {
        dataList.clear();
        this.notifyDataSetChanged();
    }

    public void insertNewData(List<String> dataList) {
        if (dataList.size() > 0) {
            this.dataList.addAll(dataList);
            this.notifyItemInserted(this.dataList.size() - 1);
        }
    }

    public void noMore() {
        isNoMore = true;
    }

    public static class CardViewHolder extends RecyclerView.ViewHolder {
        View card;
        View topLine;
        TextView runningNo;
        TextView topic;
        TextView comment;
        TextView score;
        TextView owner;
        TextView time;

        CardViewHolder(View itemView) {
            super(itemView);
            card = itemView;
            topLine = itemView.findViewById(R.id.topLine);
            runningNo = (TextView) itemView.findViewById(R.id.runningNo);
            topic = (TextView) itemView.findViewById(R.id.topic);
            comment = (TextView) itemView.findViewById(R.id.comment);
            score = (TextView) itemView.findViewById(R.id.score);
            owner = (TextView) itemView.findViewById(R.id.owner);
            time = (TextView) itemView.findViewById(R.id.time);
        }
    }

    @Override
    public TopicAdapter.CardViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.topic_item, parent, false);
        return new CardViewHolder(v);
    }

    @Override
    public void onBindViewHolder(CardViewHolder holder, int position) {
        if (dataList.size() > 0) {
            String data = dataList.get(position);
            setupDataToShow(holder, position + 1, data);
            if (position >= dataList.size() - GlobalConstants.CHECK_BEFORE_DOWNLOAD
                    && !fragment.isFetchingData() && !isNoMore) {
                // Request for more data
                fragment.insertMoreData();
            }
        }
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    private void setupDataToShow(CardViewHolder holder, Integer runningNo, String data) {
        if (data != null) {
            holder.topLine.setVisibility(runningNo == 1 ? View.GONE : View.VISIBLE);
            try {
                final JSONObject json = new JSONObject(data);
                holder.runningNo.setText(String.valueOf(runningNo));
                holder.topic.setText(json.getString("title"));
                final String url = json.isNull("url") ? "https://googl.com" : json.getString("url");
                holder.topic.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(Intent.ACTION_VIEW);
                        intent.setData(Uri.parse(url));
                        activity.startActivity(intent);
                    }
                });

                Integer score = json.isNull("score") ? 0 : json.getInt("score");
                String scoreText = score + " " + activity.getString(R.string.point) + (score > 1 ? "s" : "");
                holder.score.setText(scoreText);

                holder.owner.setText(json.getString("by"));

                holder.time.setText(prettyTime.format(new Date(System.currentTimeMillis() - json.getLong("time"))));

                Integer commentAmount = json.isNull("descendants") ? 0 : json.getInt("descendants");
                String commentText = commentAmount + " " + activity.getString(R.string.comment) + (commentAmount > 1 ? "s" : "") + (commentAmount > 0 ? " " + activity.getString(R.string.read) : "");
                holder.comment.setText(commentText);

                if (commentAmount > 0) {
                    holder.card.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            try {
                                fragment.viewComment(json.getString("title"), url, json.getString("id"));
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
}