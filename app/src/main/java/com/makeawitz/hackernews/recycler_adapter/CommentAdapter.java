package com.makeawitz.hackernews.recycler_adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.makeawitz.hackernews.MainActivity;
import com.makeawitz.hackernews.R;
import com.makeawitz.hackernews.constant.GlobalConstants;
import com.makeawitz.hackernews.fragment.CommentItemFragment;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.ocpsoft.prettytime.PrettyTime;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class CommentAdapter extends RecyclerView.Adapter<CommentAdapter.CardViewHolder>{

    private MainActivity activity;
    private List<String> dataList;
    private PrettyTime prettyTime;
    private CommentItemFragment fragment;
    private boolean isNoMore;

    public CommentAdapter(MainActivity activity, CommentItemFragment fragment) {
        this.activity = activity;
        this.fragment = fragment;
        dataList = new ArrayList<>();
        prettyTime = new PrettyTime(Locale.ENGLISH);
        isNoMore = false;
    }

    public void setDataList(List<String> dataList) {
        for (int i = 0; i < dataList.size(); i++) {
            this.dataList.add(dataList.get(i));
        }
        if (dataList.size() > 0) {
            isNoMore = false;
            this.notifyDataSetChanged();
        }
    }

    public void insertNewData(List<String> dataList) {
        for (int i = 0; i < dataList.size(); i++) {
            this.dataList.add(dataList.get(i));
        }

        if (dataList.size() > 0) {
            this.notifyItemInserted(this.dataList.size() - 1);
        }
    }

    public void noMore() {
        isNoMore = true;
    }

    public static class CardViewHolder extends RecyclerView.ViewHolder {
        View card;
        View topLine;
        TextView owner;
        TextView time;
        TextView content;
        TextView reply;

        CardViewHolder(View itemView) {
            super(itemView);
            card = itemView;
            topLine = itemView.findViewById(R.id.topLine);
            owner = (TextView) itemView.findViewById(R.id.owner);
            time = (TextView) itemView.findViewById(R.id.time);
            content = (TextView) itemView.findViewById(R.id.content);
            reply = (TextView) itemView.findViewById(R.id.reply);
        }
    }

    @Override
    public CommentAdapter.CardViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.comment_item, parent, false);
        CardViewHolder vh = new CardViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(CardViewHolder holder, int position) {
        if (dataList.size() > 0) {
            holder.topLine.setVisibility(position == 0 ? View.GONE : View.VISIBLE);
            String data = dataList.get(position);
            setupDataToShow(holder, data);
            if (position >= dataList.size() - GlobalConstants.CHECK_BEFORE_DOWNLOAD
                    && !fragment.isFetchingData() && !isNoMore) {
                // Request for more data
                fragment.insertMoreData();
            }
        }
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    private void setupDataToShow(CardViewHolder holder, String data) {
        if (data != null) {
            try {
                final JSONObject json = new JSONObject(data);
                holder.owner.setText(json.getString("by"));

                holder.time.setText(prettyTime.format(new Date(System.currentTimeMillis() - json.getLong("time"))));

                String contentText = json.getString("text");
                holder.content.setText(contentText.replaceAll("<p>", "\n\n"));

                JSONArray kids = json.isNull("kids") ? null : json.getJSONArray("kids");
                Integer commentAmount = kids == null ? 0 : kids.length();
                holder.reply.setText(commentAmount + " " + (commentAmount > 1 ? activity.getString(R.string.replies) : activity.getString(R.string.reply)));

                if (commentAmount > 0) {
                    holder.card.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            try {
                                fragment.viewComment(json.getString("id"), json.getString("by"), new CommentItemFragment());
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
}